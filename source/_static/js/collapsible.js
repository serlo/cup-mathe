$(document).ready(function(){
	// open collapsibles when clicked
	$('a.collapsible-trigger').click(function(){
		// toggle default text
		if ($(this).text() == 'Anzeigen') {
			$(this).text('Verbergen');
		} else if ($(this).text() == 'Verbergen') {
			$(this).text('Anzeigen');
		}

		// toggle right or down pointing triangle
		$(this).toggleClass('open');

		// toggle content visibility
		$(this).parent().find('div.collapsible-content').toggle();
	});
});
