$(document).ready(function() {
	$('#abstracts').find('.collapsible').find('.collapsible-content').each(function() {
		var path = $(this).parent().attr('path');
		$(this).load(path + ' #abstract div#abstract-content', 
			function() {
			MathJax.Hub.Queue(['Typeset', MathJax.Hub, 'abstracts']);
			$(this).append('<p><a href="' + path + '">Mehr zum Thema</a></p>');
		});
	});
});
