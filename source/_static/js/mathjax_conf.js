$(document).ready(function() {
	MathJax.Hub.Config({
		extensions: ["tex2jax.js"],
		jax: ["input/TeX", "output/HTML-CSS"],
		"HTML-CSS": { fonts: ["TeX"], scale: 90 },
		displayAlign: "center"
	});
});
