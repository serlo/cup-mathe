""" Loads article abstracts as collapsibles

Usage:
------

.. abstracts::

    articles/integration
    articles/maxwell-boltzmann
"""

from docutils import nodes
from docutils.parsers.rst import Directive
from sphinx.util.nodes import explicit_title_re
from collapsible import collapsible

class abstracts(nodes.General, nodes.Element):
    pass

class AbstractsDirective(Directive):
    optional_arguments = 0
    required_arguments = 0
    final_argument_whitespace = False
    has_content = True
    
    def run(self):
        env = self.state.document.settings.env

        target_id = 'abstracts'
        target_node = nodes.target('', '', ids = [target_id])


        # create node for abstracts
        node = abstracts()
        node['target_id'] = target_id

        for article in self.content:
            if not article:
                continue

            # create collapsible for article
            subnode = collapsible()

            # check for explicit titles ("Title <path_to_article>")
            # and set path and title
            explicit = explicit_title_re.match(article)

            if explicit:
                subnode['path'] = explicit.group(2)
                rawtitle = explicit.group(1)
            else:
                subnode['path'] = article
                rawtitle = u'%s' % env.titles[article]
                # remove <title> and </title>
                rawtitle = rawtitle[7:-8]

            title = nodes.paragraph()
            title.append(nodes.Text(rawtitle))
            subnode.append(title)
            
            node.append(subnode)

        return [target_node, node]

# start of abstracts in HTML
def html_visit_abstracts(self, node):
    self.body.append(self.starttag(node, 'div', CLASS = 'section'))

# end of abstracts in HTML
def html_depart_abstracts(self, node):
    self.body.append('</div>')

# don't include abstracts in LaTeX
def latex_visit_abstracts(self, node):
    pass

# don't include abstracts in LaTeX
def latex_depart_abstracts(self, node):
    pass

###############################################################################

""" Abstract for an article, can be read in other articles

Usage:
------

.. abstract::

    Abstract text.
"""

class abstract(nodes.General, nodes.Element):
    pass

class AbstractDirective(Directive):
    optional_arguments = 0
    required_arguments = 0
    final_argument_whitespace = False
    has_content = True
    
    def run(self):
        env = self.state.document.settings.env

        target_id = 'abstract'
        target_node = nodes.target('', '', ids = [target_id])

        # create node for abstract
        node = abstract()
        node['target_id'] = target_id

        # parse content
        self.state.nested_parse(self.content, self.content_offset, node)

        return [target_node, node]

# start of collapsible in HTML
def html_visit_abstract(self, node):
    self.body.append(self.starttag(node, 'div', CLASS = 'section',
        style = 'display: none'))
    self.body.append('<div id="abstract-content">')

# end of collapsible in HTML
def html_depart_abstract(self, node):
    self.body.append('</div>')
    self.body.append('</div>')

# don't include collapsible in LaTeX
def latex_visit_abstract(self, node):
    pass

# don't include collapsible in LaTeX
def latex_depart_abstract(self, node):
    pass

def setup(app):
    app.add_node(
        abstract,
        html = (html_visit_abstract, html_depart_abstract),
        latex = (latex_visit_abstract, latex_depart_abstract)
    )
    app.add_node(
        abstracts,
        html = (html_visit_abstracts, html_depart_abstracts),
        latex = (latex_visit_abstracts, latex_depart_abstracts)
    )
    app.add_directive('abstract', AbstractDirective)
    app.add_directive('abstracts', AbstractsDirective)
