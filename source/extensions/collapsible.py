""" Display collapsible text blocks in HTML output

Usage:
------

.. collapsible:: heading

    Collapsible text.

heading: string
    Heading of the collapsible text block. Block expands when heading
    is clicked. Can include math. Optional.
"""

from docutils import nodes
from docutils.parsers.rst import directives, Directive
from sphinx.util.osutil import relative_uri

class collapsible(nodes.General, nodes.Element):
    pass

class CollapsibleDirective(Directive):
    optional_arguments = 1
    required_arguments = 0
    final_argument_whitespace = True
    has_content = True
    
    def run(self):
        env = self.state.document.settings.env
 
        # create target node for insertion in the document
        target_id = 'collapsible-%d' % env.new_serialno('collapsible')
        target_node = nodes.target('', '', ids = [target_id])

        # create node for collapsible
        node = collapsible()
        node['target_id'] = target_id

        # parse heading, set 'Anzeigen' if none is given
        rawheading = nodes.paragraph()
        if not self.arguments:
            rawheading += [nodes.Text(u'Anzeigen')]
        else:
            rawheading += [nodes.Text(u'%s' % self.arguments[0])]
        self.state.nested_parse(rawheading, 0, node)

        # parse content
        self.state.nested_parse(self.content, self.content_offset, node)

        return [target_node, node]

# start of collapsible in HTML
def html_visit_collapsible(self, node):
    if 'path' in node:
        self.body.append(self.starttag(node, 'div', CLASS = 'collapsible',
            PATH = relative_uri(self.docnames[0] + '.html', 
                node['path'] + '.html')))
    else:
        self.body.append(self.starttag(node, 'div', CLASS = 'collapsible'))
    self.body.append('<a href="javascript:void(0)" ' + 
            'class="collapsible-trigger">')
    heading = node.children.pop(0)
    for child in heading:
        child.walkabout(self)
    self.body.append('</a>')    
    self.body.append('<div class="collapsible-content">')

# end of collapsible in HTML
def html_depart_collapsible(self, node):
    self.body.append('</div></div>')

# don't include collapsible in LaTeX
def latex_visit_collapsible(self, node):
    pass

# don't include collapsible in LaTeX
def latex_depart_collapsible(self, node):
    pass

def setup(app):
    app.add_node(
        collapsible,
        html = (html_visit_collapsible, html_depart_collapsible),
        latex = (latex_visit_collapsible, latex_depart_collapsible)
    )
    app.add_directive('collapsible', CollapsibleDirective)
