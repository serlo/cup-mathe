""" Display hoverboxes in HTML when mousing over text

Usage:
------

This is some :hoverbox:`text` in a paragraph. Add the
hoverbox content after the paragraph is finished.

.. hoverbox-content::

    Content of the hoverbox
"""

from docutils import nodes
from docutils.parsers.rst import directives, Directive
from sphinx.util.nodes import explicit_title_re

class hoverbox(nodes.General, nodes.Element):
    pass

class hoverbox_content(nodes.General, nodes.Element):
    pass

def hoverbox_role(name, rawtext, text, lineno, inliner, options = {},
        content = []):
    # create new node
    node = hoverbox()

    # add text to mouse over
    hoverbox_text = nodes.paragraph()

    # check for custom labels ('text <label>')
    explicit = explicit_title_re.match(text)
    if explicit:
        target_id = 'hoverbox-%s' % explicit.group(2)
        hoverbox_text.append(nodes.Text(explicit.group(1)))
    else:
        env = inliner.document.settings.env
        target_id = 'hoverbox-%d' % env.new_serialno('hoverbox')
        hoverbox_text.append(nodes.Text(text))

    node['id'] = target_id

    node.append(hoverbox_text)

    return [node], []

class HoverboxContentDirective(Directive):
    optional_arguments = 1
    required_arguments = 0
    final_argument_whitespace = False
    has_content = True

    def run(self):
        env = self.state.document.settings.env

        # create new node
        node = hoverbox_content()

        if self.arguments:
            target_id = 'hoverbox-%s-content' % self.arguments[0]
        else:
            target_id = 'hoverbox-%d-content' % env.new_serialno('hoverbox_content')

        # create target node
        target_node = nodes.target('', '', ids = [target_id])
        node['id'] = target_id

        # parse content of the hoverbox
        self.state.nested_parse(self.content, self.content_offset, node)
        
        return [target_node, node]

def html_visit_hoverbox(self, node):
    self.body.append('<span class="hoverbox-text" id="' + node['id'] + '">')

def html_depart_hoverbox(self, node):
    self.body.append('</span>')

def html_visit_hoverbox_content(self, node):
    self.body.append('<div class="hoverbox-content" id="' + node['id'] + '">')

def html_depart_hoverbox_content(self, node):
    self.body.append('</div>')

def latex_visit_hoverbox(self, node):
    pass

def latex_depart_hoverbox(self, node):
    pass

def setup(app):
    app.add_node(
            hoverbox, 
            html = (html_visit_hoverbox, html_depart_hoverbox),
            latex = (latex_visit_hoverbox, latex_depart_hoverbox)
    )
    app.add_node(
            hoverbox_content, 
            html = (html_visit_hoverbox_content, html_depart_hoverbox_content),
            latex = (latex_visit_hoverbox, latex_depart_hoverbox)
    )
    app.add_role('hoverbox', hoverbox_role)
    app.add_directive('hoverbox-content', HoverboxContentDirective)
