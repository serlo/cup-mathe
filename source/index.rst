.. math@cup.lmu documentation master file, created by
   sphinx-quickstart on Sun Jul 29 12:59:49 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. _master_doc:

Mathe-Lernplattform der Fakultät für Chemie und Pharmazie
#########################################################

Herzlich willkommen!
====================

Du befindest Dich auf der Mathe-Lernplattform der Fakultät für Chemie und
Pharmazie der LMU München. Auf dieser Seite wollen wir Dir mit umfangreichen
Artikeln, Animationen und interaktiven Grafiken die mathematischen
Werkzeuge an die Hand geben, die Du in Naturwissenschaften wie Chemie,
Biochemie und Pharmazie benötigst. Diese wollen wir anhand intuitiver
Einführungen und ausführlicher Beispiele verständlich und zugänglich machen
und Dir so die Schönheit der Mathematik näherbringen.

.. toctree::
    :maxdepth: 1
    :caption: Inhalte:

    articles/index
    lectures/index

.. toctree::
    :includehidden:

    Kopieren – aber richtig! <doc/CC-BY-SA>

.. toctree::
    :hidden:
    
    doc/authors-guide
