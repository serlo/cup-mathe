.. _integration:

Integration
###########

.. toctree::
    :caption: Inhaltsverzeichnis
    :maxdepth: 2

.. abstract::

    Mit einem Integral lässt sich die Fläche zwischen einer Funktion
    und der Ordinatenachse bestimmen. In einem Intervall :math:`[a, b]` 
    entspricht diese Fläche der Änderung des Funktionswerts ihrer 
    *Stammfunktion*. 
    
    Zu einer Funktion :math:`f` mit Definitionsbereich :math:`D` nennt man 
    eine Funktion :math:`F` Stammfunktion von :math:`f`, wenn sie die 
    Bedingung

    .. math::
        F'(x) = f(x)

    
    für alle :math:`x \in D` erfüllt. Die Stammfunktion ist dabei nur bis
    auf eine additive Konstante festgelegt.

    Die Lösung des Integrals ist dann gegeben durch

    .. math::
        \int_a^b f(x)\,\mathrm{d}x = \left[F(x)\right]_a^b = F(b) - F(a)

    
    für bestimmte bzw.

    .. math::
        \int f(x)\,\mathrm{d}x = F(x) + C ~ (C \in \mathbb{R})

    
    für unbestimmte Integrale (*Hauptsatz der Differential- und
    Integralrechnung*).

    Wichtige bzw. häufig auftretende Stammfunktionen sind:

    .. csv-table::
        :header: ":math:`f(x)`", ":math:`F(x) ~ (+ C)`", "Anmerkung"

        ":math:`k`", ":math:`kx`", ":math:`k \in \mathbb{R}`"
        ":math:`x^n`", ":math:`\frac{1}{n+1} x^{n+1}`", ":math:`n \in 
        \mathbb{R} \backslash \{-1\}`"
        ":math:`\frac1x = x^{-1}`", ":math:`\ln(x)`", ":math:`x \in
        \mathbb{R}^+`"
        ":math:`\frac1x = x^{-1}`", ":math:`\ln(|x|)`", ":math:`x \in
        \mathbb{R}`"
        ":math:`e^{\alpha x}`", ":math:`\frac1\alpha e^{\alpha x}`", "
        :math:`\alpha \in \mathbb{R} \backslash \{0\}`"
        ":math:`\ln(x)`", ":math:`x \ln(x) - x`"
        ":math:`\sin(x)`", ":math:`-\cos(x)`"
        ":math:`\cos(x)`", ":math:`\sin(x)`"
        ":math:`\sinh(x)`", ":math:`\cosh(x)`"
        ":math:`\cosh(x)`", ":math:`\sinh(x)`"
        ":math:`\frac{1}{\sqrt{1-x^2}}`", ":math:`\arcsin(x)`", ":math:`|x| 
        < 1`"
        ":math:`-\frac{1}{\sqrt{1-x^2}}`", ":math:`\arccos(x)`", ":math:`|x| 
        < 1`"
        ":math:`\frac{1}{1+x^2}`", ":math:`\arctan(x)`"


Integrale – Einführung und Motivation
=====================================

Viele der Zusammenhänge in der Physik verbinden drei 
physikalische Größen miteinander. Im einfachsten Fall ist dann eine 
der drei Größen direkt proportional zu den beiden anderen.

.. collapsible:: Beispiel 1: :math:`s=vt`

    Erklärung: Bewegt sich ein Körper mit konstanter 
    Geschwindigkeit :math:`v`, so ist der zurückgelegte Weg :math:`s` 
    umso größer, je schneller die Geschwindigkeit :math:`v` und die 
    verstrichene Zeit :math:`t` ist. Genauer: Fährt ein Auto doppelt
    so schnell wie ein anderes, dann kommt es in der gleichen 
    Zeit doppelt so weit wie das andere, es gilt also 
    :math:`s\sim v`. Fahren zwei Autos gleich schnell, so kommt ein 
    Auto, das doppelt so lang fährt, auch doppelt so weit, es 
    gilt also :math:`s\sim t`. Insgesamt gilt also: :math:`s=vt`.

    .. plot:: _static/pyplot/distance-speed-time.py

.. collapsible:: Beispiel 2: :math:`W=Fs`

    Erklärung: Wir suchen nach einer sinnvollen Definition 
    der physikalischen Größe Arbeit :math:`W`. Ein Körper soll um 
    eine bestimmte Strecke :math:`s` verschoben werden, hierbei wird
    nach unserem alltäglichen Verständnis Arbeit verrichtet. 
    Wovon hängt es nun ab, wie viel Arbeit verrichtet wird? Die 
    Ursache unserer Anstrengung ist klar eine Kraft, die uns 
    entgegenwirkt: Könnten wird die Reibungskraft, die in diesem
    Fall wirkt, vollständig abschalten, dann würde der Körper 
    durch einen minimalen Stups eine beliebig große Strecke 
    zurücklegen, ohne weitere Anstrengung. 
    Je größer die Kraft ist, die uns entgegenwirkt, umso mehr 
    Anstrengung kostet es, den Körper zu verschieben. Es 
    erscheint sinnvoll, die Größe :math:`W` proportional zur zu 
    überwindenden Kraft :math:`F` zu wählen, :math:`W\sim F`. Schiebt 
    man nun den Körper um eine doppelte Strecke :math:`s`, so 
    verrichtet man klar die doppelte Arbeit, also auch :math:`W\sim s`. 
    Insgesamt also :math:`W=Fs`.

.. todo::
    Bild: Geleistete Arbeit im s-F-Diagramm (als rechteckige Fläche unter dem Graphen)

In beiden Beispielen wurde angenommen, dass die beiden Faktoren 
unabhängig voneinander sind. Was passiert aber, wenn die 
Momentangeschwindigkeit abhängig vom Zeitpunkt ist, also :math:`v=v(t)`?
Gilt dann einfach :math:`s(t)=v(t)\cdot t`? Analog: Was passiert, wenn 
die Kraft :math:`F` sich im Verlauf des Wegs verändert, also :math:`F=F(s)` 
ist, wie kann dann die Arbeit berechnet werden?

Ein Gegenbeispiel zeigt schnell, dass der Ansatz 
:math:`s(t)=v(t)\cdot t` nicht richtig sein kann.
Ist zum Beispiel :math:`v(t)=a\cdot t` mit 
:math:`a=1\,\mathrm{\frac{m}{s^2}}` (das Auto wird im Verlauf der Zeit 
immer schneller, beginnend bei Null), dann wäre 
:math:`s(t)=v(t)\cdot t=(a\cdot t)\cdot t=a\cdot t^2` und nach 
:math:`t=1\,\mathrm{s}` wäre die zurückgelegte Strecke 
:math:`s(1\,\mathrm{s})=1\,\mathrm{m}`. Die gleiche Strecke legt aber auch ein
Auto zurück, das von Anfang an mit konstanter Geschwindigkeit 
:math:`v=1\,\mathrm{\frac{m}{s}}` fährt. Intuitiv erwartet man, dass das 
zweite Auto weiter kommt als das erste, da es zu jedem Zeitpunkt
schneller ist. Eine Formel :math:`s(t)=v(t)\cdot t` kann also nicht 
richtig sein.

Wie lässt sich der richtige Zusammenhang finden? Dafür unterteilt
man das Zeitintervall :math:`[t_\mathrm{Anfang}, t_\mathrm{Ende}]` in viele 
kleine Abschnitte, wobei jeder Abschnitt so klein ist, dass dort die
Geschwindigkeit nahezu konstant ist. In einem solchen Abschnitt 
:math:`[t_i,t_i+\Delta t]`, kann die Geschwindigkeit als näherungsweise 
konstant angenommen werden, sodass die Formel für konstante 
Geschwindigkeit anwendbar ist:

.. math::
    \Delta s_i = v(t_i)\Delta t

Um die insgesamt zurückgelegte Strecke zu finden, müssen alle 
Teilstrecken aufsummiert werden:

.. math::
    s = \sum_i \Delta s_i = \sum_i v(t_i)\Delta t

Macht man die Abschnitte :math:`[t_i,t_i+\Delta t]` immer kleiner, so 
wird die Abweichung vom wahren Wert immer geringer:

.. raw:: html
    :file: ../../_static/animations/riemann-integral.html

Wir sehen, dass der wahre Wert der zurückgelegten Strecke der Fläche
zwischen dem Graphen und der Rechtsachse entspricht. Diesen Wert 
nennt man das Integral der Geschwindigkeit nach der Zeit und 
schreibt ihn als

.. math::
    s=\int_{s_\mathrm{Anfang}}^{s_\mathrm{Ende}} \,\mathrm{d}s = 
    \int_{t_\mathrm{Anfang}}^{t_\mathrm{Ende}} v(t)\,\mathrm{d}t

Das Integrationszeichen ist ein stilisiertes S (für Summe), da die 
Integration eigentlich nur die Summation kleiner Rechtecksflächen 
über unendlich kleinen Abschnitten (infinitesimal kleinen 
Intervallen) darstellt. Der Summationsindex :math:`i` fällt weg, weil 
die Anzahl der Abschnitte nun nicht mehr gezählt werden kann 
(überabzählbar ist). Es werden nur noch die Grenzen des 
Gesamtintervalls :math:`t_\mathrm{Anfang}` und :math:`t_\mathrm{Ende}` 
angegeben.


Wir sehen aus den vorangehenden Überlegungen: Die Motivation für Integrale 
kommt aus der Physik. Immer dann, wenn wir eine physikalische Größe gerne 
als Produkt zweier anderen Größen schreiben würden, das jedoch nicht möglich
ist, weil eine von ihnen von der anderen abhängt, kommt ein Integral ins 
Spiel. So ist die zurückgelegte Strecke :math:`s` das Integral über die 
Geschwindigkeit :math:`v(t)` nach der Zeit :math:`t`, sobald die 
Geschwindigkeit von der Zeit abhängt, und somit der Produktansatz 
:math:`s=v\cdot t` nicht mehr anwendbar ist.


Integrale – Stammfunktion und Lösung
====================================

Es stellt sich nun die Frage: Wie lassen sich Integrale ausführen? Das 
Aufsummieren von unendlich vielen Rechtecksflächen sieht nicht sehr 
praktikabel aus. Formulieren wir das Problem deshalb einmal um, indem wir
uns fragen: Wenn wir an jedem Punkt die Änderungsrate einer Funktion kennen,
was ist dann die Gesamtänderung des Funktionswertes in einem bestimmten
Intervall? Die momentane Änderung einer Funktion ist bekanntlich durch ihre
erste Ableitung festgelegt. Besonders intuitiv ist dieser Zusammenhang bei
Funktionen, die von der Zeit abhängen, wie etwa unser Beispiel mit
zurückgelegter Strecke und Geschwindigkeit. Definieren wir uns dazu eine 
Funktion :math:`s(t)`, die die zum Zeitpunkt :math:`t` zurückgelegte Strecke
angibt. Der Zusammenhang zwischen :math:`s(t)` und :math:`v(t)` ist schnell 
ersichtlich:

.. math::
    \dot{s}(t) = \frac{\mathrm{d}}{\mathrm{d}t}s(t) = v(t)

:math:`v(t)` ist also die Änderungsrate von :math:`s(t)` mit der Zeit. Da 
diese Relation zu jedem Zeitpunkt :math:`t` gültig ist, nennt man 
:math:`s(t)` auch *Stammfunktion* von :math:`v(t)`.

Wie lässt sich also die Änderung der Strecke in einem Zeitintervall
:math:`[t_1, t_2]` bestimmen? Die Antwort darauf liefert uns schließlich
das Integral über die Geschwindigkeit:

.. math::
    s(t_2) - s(t_1) = \int_{t_1}^{t_2} v(t)\,\mathrm{d}t

oder andersherum formuliert:

.. math::
    \int_{t_1}^{t_2} v(t)\,\mathrm{d}t = s(t_2) - s(t_1)

Die Lösung des Integrals ist also durch die Differenz des Funktionswertes
der Stammfunktion am End- und Startpunkt der Integration gegeben. Diesen
Zusammenhang nennt man den *Hauptsatz der Differential- und 
Integralrechnung* oder kurz HDI.

Abstrahieren wir dieses Konzept nun etwas: Zu einer reellen, stetigen 
Funktion :math:`f` mit Definitionsbereich :math:`D` heißt eine Funktion 
:math:`F` *Stammfunktion* von :math:`f`, wenn für alle :math:`x \in D` gilt:

.. math::
    F'(x) = f(x)

.. collapsible:: **Anmerkung:** Eine Stammfunktion ist nicht eindeutig.

    Beim Ableiten fallen additive Konstanten weg; ist also :math:`F` eine 
    Stammfunktion einer Funktion :math:`f`, dann ist auch :math:`\tilde F = F+c`
    eine Stammfunktion von :math:`f`, denn 

    .. math::
        \tilde F'(x)=(F+c)'(x)=F'(x)=f(x)

    Beispiel: :math:`F(x)=x^2-3` ist eine Stammfunktion von :math:`f(x)=2x`,
    denn :math:`F'(x)=2x`. Aber auch :math:`\tilde F(x)=x^2+5` ist eine 
    Stammfunktion von :math:`f(x)`. Es gilt :math:`\tilde F(x)=F(x)+8`.

Das Integral über ein Intervall :math:`[a, b]` lässt sich nun folgendermaßen
bestimmen:

.. math::
    \int_a^b f(x)\,\mathrm{d}x = \left[F(x)\right]_a^b = F(b) - F(a)

Mit dem HDI haben wir also ein Werkzeug, um Integrale auszuführen, ohne
unendliche Summen berechnen zu müssen. Es reicht aus, eine Stammfunktion
der zu integrierenden Funktion zu finden.

Bestimmte und unbestimmte Integrale
===================================

Noch ein kleiner Einschub, bevor wir uns der Suche nach Stammfunktionen widmen
können: Bislang haben wir uns ausschließlich Integrale über ein bestimmtes
Intervall, z.B. :math:`[a, b]` oder :math:`[t_1, t_2]` angesehen. Diese 
Integrale nennt man *bestimmte* Integrale und ihr Ergebnis ist immer eine
Zahl (sofern sie nicht divergieren). Ein *unbestimmtes* Integral hingegen 
liefert als Ergebnis die Stammfunktion der zu integrierenden Funktion:

.. math::
    \int f(x)\,\mathrm{d}x = F(x) + C~(C \in \mathbb{R})

Da die Stammfunktion einer Funktion nur bis auf eine additive Konstante
festgelegt ist (s.o.), fügt man diese additive Konstante in Form einer nicht
näher festgelegten reellen Zahl :math:`C` hinzu.

.. collapsible:: Beispiel: :math:`\int x^2 \mathrm{d}x`

    .. math::
        \int x^2 \mathrm{d}x = \frac13 x^3 + C

    Überzeuge Dich durch Ableiten selbst davon, dass diese Funktion eine
    Stammfunktion von :math:`x^2` ist.

Spielt die Wahl von :math:`C` zur Berechnung bestimmter Integrale eine
Rolle? **Nein!** Durch die Bildung der Differenz kürzt sich :math:`C`
nämlich einfach weg:

.. math::
    \int_a^b f(x)\,\mathrm{d}x &= \left[F(x) + C\right]_a^b \\ 
    &= (F(b) + C) - (F(a) + C) \\
    &= F(b) + C - F(a) - C \\
    &= F(b) - F(a)

Nun also zur eigentlichen Lösung eines Integrals. Das Aufstellen 
der Stammfunktion erweist sich nicht immer als trivial, aber es gibt
eine Reihe von Tricks, die diese Herausforderung vereinfachen.

Für manche Funktionen wie Potenzfunktionen lässt sich die 
Stammfunktion sehr einfach bestimmen, indem man sich fragt: "Welche 
Funktion ergibt abgeleitet :math:`f(x)`?" Hier ist eine Übersicht von 
Integralen solcher einfacher Funktionen:


.. csv-table::
    :header: ":math:`f(x)`", ":math:`F(x)`", "Anmerkung"

    ":math:`x^n`", ":math:`\frac{1}{n+1}x^{n+1} + C`", "falls :math:`n \in 
    \mathbb{R}\backslash\{-1\}`"
    ":math:`\frac1x`", ":math:`\ln(|x|) + C`"
    ":math:`e^x`", ":math:`e^x + C`"
    ":math:`\sin(x)`", ":math:`-\cos(x) + C`"
    ":math:`\cos(x)`", ":math:`\sin(x) + C`"

Stammfunktionen zusammengesetzter Funktionen
============================================

Häufig hat man es mit komplexeren, zusammengesetzten Funktionen zu tun.
Wie kann für eine solche Funktion die Stammfunktion gefunden werden?

Nun da wir wissen, dass das Integrieren die inverse Operation zum
Ableiten ist, liegt es nahe, dass sich Ableitungsregeln aufs Integrieren
übertragen lassen.

Die folgende Tabelle zeigt eine Liste der Ableitungsregeln und der 
zugehörigen Integrationsregeln:

.. csv-table::
    :header: "Ableitungsregel", "Zugehörige Integrationsregel"
    
    "Additivität der Ableitung :math:`\displaystyle(f+g)'(x) = f'(x)+g'(x)`
    ","Additivität des Integrals :math:`\displaystyle\int_a^b 
    (f(x)+g(x))\,\mathrm{d}x = \int_a^b f(x)\,\mathrm{d}x + 
    \int_a^b g(x)\,\mathrm{d}x`"
    "Homogenität der Ableitung :math:`\displaystyle(c\cdot f)'(x) = c\cdot 
    f'(x)`","Homogenität des Integrals :math:`\displaystyle\int_a^b c\cdot 
    f(x)\,\mathrm{d}x = c\cdot \int_a^b f(x)\,\mathrm{d}x`"
    "Produktregel :math:`\displaystyle(f\cdot g)'(x) = f'(x)\cdot g(x) + 
    f(x) \cdot g'(x)`","Partielle Integration :math:`\displaystyle\int_a^b 
    f’(x)\cdot g(x)\,\mathrm{d}x  = [f(x)\cdot g(x)]_a^b - \int_a^b f(x)\cdot 
    g’(x)\,\mathrm{d}x`"
    "Kettenregel :math:`\displaystyle(f\circ g)'(x) = f'(g(x))\cdot g'(x)`
    ","Integration mittels Substitution :math:`\displaystyle\int_a^b 
    f(g(x))\cdot g’(x)\,\mathrm{d}x = \int_{g(a)}^{g(b)}f(g(x))\,\mathrm{d}g`"


Die Integrationsregeln werden im Folgenden aus den Ableitungsregeln 
hergeleitet und ihre Anwendung durch einige Beispiele illustriert.

Auseinander- und Zusammenziehen von Summen (Additivität des Integrals)
______________________________________________________________________

Für Ableitungen gilt: Die Ableitung einer Summe zweier Funktionen ist 
die Summe ihrer Ableitungen. Ist also eine Funktion :math:`H` die Summe der 
Funktionen :math:`F` und :math:`G`, sodass also :math:`H(x)=F(x)+G(x)` gilt,
dann ist auch ihre Ableitung gleich der Summe der Ableitungen von :math:`F` 
und :math:`G`:


.. math::
    H'(x) = (F+G)'(x) = F'(x)+G'(x)

Nutzen wir nun diese Eigenschaft der Ableitungen, um die zugehörige 
Eigenschaft des Integrals zu finden. Wir suchen die Stammfunktion zur 
Funktion :math:`h(x)=f(x)+g(x)`, wobei die Stammfunktion von :math:`f` durch
:math:`F` und die von :math:`g` durch :math:`G` gegeben ist. Es drängt sich
der Gedanke auf, dass die Stammfunktionen :math:`H` von :math:`h` mittels

.. math::
    H(x) = (F+G)(x) = F(x)+G(x)

bestimmt werden kann. Damit würde gelten:

.. math::
    \int (f(x) + g(x))\,\mathrm{d}x = \int f(x)\,\mathrm{d}x + 
    \int g(x)\,\mathrm{d}x

Wichtig: An dieser Stelle ist es lediglich ein Ansatz, der sinnvoll 
erscheint, aber nicht notwendigerweise richtig sein muss! Wir können 
jedoch leicht beweisen, dass er richtig ist; dafür müssen wir lediglich 
zeigen, dass :math:`H` die definierende Eigenschaft einer Stammfunktion, 
:math:`H'(x)=h(x)` erfüllt. Das ist tatsächlich der Fall, denn

.. math::
    H'(x) = F'(x)+G'(x) = f(x)+g(x) = h(x)

:math:`H` ist also tatsächlich eine Stammfunktion von :math:`h`!

Betrachten wir einige Beispiele für das Auseinander- und Zusammenziehen
von Summen:

.. collapsible:: Beispiel 1: :math:`\displaystyle I(x) = \int (x^2+x+1)\,\mathrm{d}x`
    
    .. math::
        I(x) &= \int (x^2+x+1)\,\mathrm{d}x \\
        &= \int x^2\,\mathrm{d}x + \int x\,\mathrm{d}x + \int 1\,\mathrm{d}x \\
        &= \frac13 x^3 + \frac12 x^2 + x + C
    
    Hier konnten wir das Integral über eine zusammengesetzte 
    Funktion in Integrale über einfache Funktionen aufzuspalten.


.. collapsible:: Beispiel 2: :math:`\displaystyle I(x) = \int \frac{4x+4}{(x+2)^2}\,\mathrm{d}x + \int \frac{x^2}{(x+2)^2}\,\mathrm{d}x`

    .. math::
        I(x) &= \int \frac{4x+4}{(x+2)^2}\,\mathrm{d}x + \int 
        \frac{x^2}{(x+2)^2}\,\mathrm{d}x \\
        &= \int \left( \frac{4x+4}{(x+2)^2} + 
        \frac{x^2}{(x+2)^2} \right)\,\mathrm{d}x \\
        &= \int \frac{x^2+4x+4}{(x+2)^2}\,\mathrm{d}x \\
        &= \int 1\,\mathrm{d}x \\
        &= x + C
    
    In diesem (zugegebenermaßen recht konstruierten) Beispiel 
    ließ sich das Integral über die Summe zweier Integranden viel 
    einfacher berechnen, als über die beiden Integranden getrennt.

    Zusätzliche Frage: Was ist der Definitionsbereich von :math:`I(x)`?


Heraus- und Hineinziehen von Konstanten (Homogenität des Integrals)
___________________________________________________________________

Für Ableitungen gilt: Konstante Vorfaktoren können aus einer Ableitung 
herausgezogen werden. Ist also :math:`G(x) = c\cdot F(x)` für Funktionen :math:`F` 
und :math:`G` sowie eine Konstante :math:`c`, dann gilt:

.. math::
    G'(x) = (c\cdot F)'(x) = c\cdot F'(x)

Nutzen wir nun diese Eigenschaft der Ableitungen, um die zugehörige 
Eigenschaft des Integrals zu finden. Wir suchen die Stammfunktion zur 
Funktion :math:`g(x)=c\cdot f(x)`, wobei die Stammfunktion von :math:`f` durch 
:math:`F` gegeben ist. Nun könnte man auf die Idee kommen, dass die 
Stammfunktionen :math:`G` von :math:`g` mittels

.. math::
    G(x) = (c\cdot F)(x) = c\cdot F(x)

bestimmt werden kann. Damit würde gelten:

.. math::
    \int c\cdot f(x)\,\mathrm{d}x = c\cdot \int f(x)\,\mathrm{d}x

Beweisen wir, dass unser Ansatz für die Stammfunktion von :math:`g` 
tatsächlich richtig ist. Das ist der Fall, denn

.. math::
    G'(x) = (c\cdot F)'(x)= c\cdot F'(x) = c\cdot f(x) = g(x)

:math:`G` ist also tatsächlich eine Stammfunktion von :math:`g`.

Betrachten wir nun einige Beispiele für das Herausziehen von 
multiplikativen Konstanten. Zunächst ist es sinnvoll, multiplikative 
Konstanten zu identifizieren, welche nicht von der Integrationsvariablen
abhängen. Diese können dann vor das Integral gezogen werden. 

.. collapsible:: Beispiel 1: :math:`f(x) = 2x`

    .. math::
        \int_a^b f(x)\,\mathrm{d}x = \int_a^b 2x\,\mathrm{d}x = 
        2\int_a^b x\,\mathrm{d}x

.. collapsible:: Beispiel 2: :math:`g(x, y) = x^2\cdot y^2`

    .. math::
        \int_a^b g(x, y)\,\mathrm{d}y = \int_a^b x^2\cdot 
        y^2\,\mathrm{d}y = x^2\cdot \int_a^b y^2\,\mathrm{d}y
        
    Diese Separation ist nur gültig, wenn :math:`x` nicht von :math:`y` 
    abhängt!


Für schwierigere Integrale gibt es zwei Techniken, die beim
Lösen hilfreich sind: Partielle Integration und Integration durch
Substitution. Beide Techniken lösen das Integral nicht, sondern
formen es in ein anderes Integral um, das – so die Hoffnung –
einfacher zu lösen ist. Sie basieren auf den Ableitungsregeln: 
Die partielle Integration folgt aus der Produktregel, die Substitution 
aus der Kettenregel.

Partielle Integration
_____________________

Die partielle Integration bietet eine Möglichkeit, das Integral
eines Produkts zweier Funktionen zu bestimmen. Die Formel leitet
sich – wie bereits erwähnt – aus der Produktregel ab.

Die Produktregel für Ableitungen besagt:

.. math::
    (f\cdot g)’(x) = f’(x)\cdot g(x) + f(x)\cdot g’(x)

Integrieren wir nun auf beiden Seiten über :math:`x` im Intervall
:math:`[a,b]`, dann erhalten wir

.. math::
    \int_a^b (f\cdot g)’(x)\,\mathrm{d}x = \int_a^b f’(x)\cdot 
    g(x)\,\textrm{d}x + \int_a^b f(x)\cdot g’(x)\,\mathrm{d}x

Die linke Seite lässt sich ausintegrieren, da im Integranden eine 
totale Ableitung steht:

.. math::
    [f(x)\cdot g(x)]_a^b = \int_a^b f’(x)\cdot g(x)\,\mathrm{d}x + 
    \int_a^b f(x)\cdot g’(x)\,\mathrm{d}x

Nun können wir die Gleichung nach dem ersten Integral auflösen, 
indem wir das zweite Integral auf die andere Seite bringen:

.. math::
    \int_a^b f’(x)\cdot g(x)\,\mathrm{d}x = [f(x)\cdot g(x)]_a^b - \int_a^b 
    f(x)\cdot g’(x)\,\mathrm{d}x


Dies ist die Formel der partiellen Integration. Sie ist unter 
folgenden Umständen hilfreich:


* Der Integrand lässt sich als Produkt zweier Funktionen :math:`f’(x)`
  und :math:`g(x)` schreiben.
* Für eine der Funktionen (nämlich :math:`f’(x)`) ist die 
  Stammfunktion :math:`f(x)` bekannt.
* Das Integral :math:`\int f(x)\cdot g’(x)\,\mathrm{d}x` ist einfacher zu 
  berechnen als das ursprüngliche Integral 
  :math:`\int f’(x)\cdot g(x)\,\mathrm{d}x`.

Einige Beispiele zur Anwendung der partiellen Integration:

.. collapsible:: Beispiel 1: :math:`\int \sin^2(x)\,\mathrm{d}x`

    Zur Erinnerung:
    
    .. math::
       \int u'(x)\cdot v(x)\,\mathrm{d}x = u(x)\cdot v(x) - 
       \int u(x)\cdot v'(x)\,\mathrm{d}x

    Wir wählen :math:`u'(x) = \sin(x)` und :math:`v(x) = \sin(x)`, 
    demnach ist :math:`u(x) = -\cos(x)` und :math:`v'(x) = \cos(x)`.
    Unser Integral vereinfacht sich also zu

    .. math::
        \int \sin^2(x)\,\mathrm{d}x &= -\sin(x)\cos(x) - \int 
        -\cos^2(x)\,\mathrm{d}x \\
        &= -\sin(x)\cos(x) + \int \cos^2(x)\,\mathrm{d}x \\
        &= -\sin(x)\cos(x) + \int (1-\sin^2(x))\,\mathrm{d}x \\ 
        &= -\sin(x)\cos(x) + \int 1\,\mathrm{d}x - \int 
        \sin^2(x)\,\mathrm{d}x \\
        &= -\sin(x)\cos(x) + x - \int \sin^2(x)\,\mathrm{d}x \\
        \Leftrightarrow 2\int \sin^2(x)\,\mathrm{d}x &= x-\sin(x)\cos(x) \\
        \Leftrightarrow \int \sin^2(x)\,\mathrm{d}x &= 
        \frac{x-\sin(x)\cos(x)}{2} + C \\
        &= \frac{x}{2} - \frac{\sin(2x)}{4} + C

.. todo::
    Zweites Beispiel für partielle Integration

.. todo::
    Herleitung Substitution

.. todo::
    Beispiele für Substitution
