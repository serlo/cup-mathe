.. _authors-guide:

.. toctree::
    :caption: Inhaltsverzeichnis
    :maxdepth: 2

Guide for authors
#################

Introduction
============

This guide lists some of the available markup elements in Sphinx's ``.rst`` 
files.

Markup can be controlled either using *roles* (inline markup, similar to 
``$...$`` or ``\textbf{...}`` commands in :math:`\LaTeX`) or *directives* 
(which define an environment, similar to ``\begin{equation}...\end{equation}`` 
in :math:`\LaTeX`).

Roles are written as ``:rolename:`content``` directly inside the text. Note 
that there is no whitespace between the ``:rolename:`` and the ```content```. 
Also note that there must be whitespaces before ``:rolename:`` and after 
```content```.

Directives are written as

.. code-block:: rest

    .. directive:: argument(s)
        :option: option value

        content

Note that there is an empty line between the directive and the content, also 
the content is indented. Some directives require one or several arguments, 
some do not.

Standard markup
===============

The following commands are available in the default Sphinx installation.
See http://www.sphinx-doc.org/en/master/usage/restructuredtext/basics.html
for further reading.

Comments
--------

To put a comment, use an empty directive, that is, only two dots:

.. code-block:: rest

    ..
        This is a comment
        It can also span several lines
        
Headlines
---------

To put a headline, underline it:

.. code-block:: rest

    This is a first level headline
    ##############################

    This is a second level headline
    ===============================

    This is a third level headline
    ------------------------------

    This is a fourth level headline
    ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The length of the underline must at least be that of the underlined text!

Lists
-----

There are many options to create unordered and ordered lists. For unordered lists, use:

.. code-block:: rest

    * This is a single bullet point
        * You can also nest lists
    * This will appear on the first level. The item
      spans two lines (notice the indentation)

For ordered lists, you have a number of options:

.. code-block:: rest

    1. This is the first item.
    2. This is the second item.
    
       a. Nested numbered lists also work.
       #. You can tell Sphinx to carry on the enumeration by itself.
       #. This will be labelled "c.".

:math:`\TeX` code
-----------------
To put inline math, use ``:math:`your TeX code```. For displaymath, use

.. code-block:: rest

    .. math::
        your TeX code
    
Embedding HTML files
--------------------

To embed an HTML file, use

.. code-block:: rest
    
    .. raw:: html
        :file: path/to/file.html
  
where ``path/to/file.html`` is the relative path to your HTML file.


Custom markup
=============

Collapsibles
------------

To put a collapsible box with a clickable heading, use

.. code-block:: rest

    .. collapsible:: Heading

        content
    
To put a collapsible box with a clickable "Show"/"Hide" heading, leave the
heading empty, that is, use

.. code-block:: rest

    .. collapsible::

        content

Abstracts
---------

An article can be given an abstract which is invisible in the article itself,
but can be loaded as a collapsible in other documents.

Creation of the abstract:

.. code-block:: rest
    
    .. abstract::

        Lorem ipsum dolor sit amet, ...

Loading the abstract in another document:

.. code-block:: rest

    .. abstracts::

        articles/math/integration
        Custom Title <articles/math/differentiation>

The first option will load the abstract of articles/math/integration.rst and 
grab that document's title, while the second option will insert the abstract 
of articles/math/differentiation.rst but use the title "Custom Title" for the
collapsible.

The result will look like this:

.. abstracts::

    articles/math/integration
    Custom Title <articles/math/differentiation>
    
Hoverboxes
----------

Hoverboxes (a.k.a. tooltips) can be inserted into the document using a
combination of roles and directives:

.. code-block:: rest

    Lorem ipsum dolor :hoverbox:`sit` amet, consetetur sadipscing elitr, 
    sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam 
    erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et 
    ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem 
    ipsum dolor sit amet.

    .. hoverbox-content::

        This is some additional info on the word *sit*.

The result will look like this:

Lorem ipsum dolor :hoverbox:`sit` amet, consetetur sadipscing elitr, 
sed diam eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed 
diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet 
clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.

.. hoverbox-content::

    This is some additional info on the word *sit*.

You also have the option to label hoverboxes and their content:

.. code-block:: rest

    This is some :hoverbox:`text <label1>`. It shows you how to use 
    :hoverbox:`hoverboxes <label2>` with labels.
    
    .. hoverbox-content:: label2

        This content will be matched to the text with label "label2".

    .. hoverbox-content:: label1

        This content will be matched to the text with label "label1".

If no label is given, the hoverboxes and their content are enumerated in 
order, meaning that the first unlabelled ``:hoverbox:`` role will contain the 
content of the first unlabelled ``.. hoverbox-content::`` directive, the 
second that of the second and so on. Note that role content cannot be parsed, 
so the text the user hovers can only be raw text without formatting and 
especially no mathematical formulae.

Matplotlib embedding
--------------------

To embed a matplotlib plot, use

.. code-block:: rest

    .. plot:: path/to/plot.py
    
where ``path/to/plot.py`` is the relative path to the plot file. The plot can 
also be generated in the directive itself using matplotlib syntax:

.. code-block:: rest

    .. plot::

        import numpy as np
        import matplotlib.pyplot.plot as plt

        x = np.arange(0, 5, 0.01)
        plt.plot(x, x*x)
        plt.show()

For options, see the `matplotlib documentation <https://matplotlib.org/>`_.

Glowscript embedding
--------------------

VPython animations created with Glowscript can be exported as HTML and 
embedded on this platform. To embed a glowscript file, use

.. code-block:: rest

    .. raw:: html
        :file: path/to/file.html

where ``path/to/file.html`` is the path to your HTML file exported from 
Glowscript. However, there's a few things you need to watch out for: In your 
Glowscript / VPython source code, you **need** to include

.. code-block:: python
    
    MathJax.Hub.Config({
        "extensions": ["tex2jax.js"],
        "jax": ["input/TeX", "output/HTML-CSS"],
        "HTML-CSS": { "fonts": ["TeX"] },
        "displayAlign": "center"
    })

as well as

.. code-block:: python

    MathJax.Hub.Queue(["Typeset", MathJax.Hub])

or math content will not render correctly. Glowscript files also come with
their own stylesheet, so you need to comment out or delete line 4 of the 
Glowscript html file.
